import 'dart:ffi';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Map data = {};

  @override
  Widget build(BuildContext context) {
    data = data.isNotEmpty ? data : ModalRoute.of(context)!.settings.arguments as Map;

    var bgImage = data['isDaytime'] ? 'day.png' : 'night.png';
    var bgColor = data['isDaytime'] ? Color.fromARGB(255,220,254,255): Color.fromARGB(255, 22, 72, 118);

    return Scaffold(
      backgroundColor: bgColor,
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(image: 
            AssetImage('imgs/$bgImage'),
            fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0,120,0,0),
            child: Column(
              children: [
                TextButton.icon(
                  onPressed: () async {
                    dynamic result = await Navigator.pushNamed(context, '/location');
                    setState(() {
                      data = {
                        'time' : result['time'],
                        'location' : result['location'],
                        'flag' : result['flag'],
                        'isDaytime' : result['isDaytime'],
                      };
                    });
                  },
                  icon: Icon(
                    Icons.edit_location,
                    color: Colors.grey[300]),
                  label: Text('Edit Location',
                  style: TextStyle(
                    color: Colors.grey[300],
                    ),
                  ),
                ),
                SizedBox(height: 20.0,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(data["location"],
                    style: TextStyle(
                      fontSize: 28.0,
                      letterSpacing: 2.0,
                      color: Color.fromARGB(255, 180, 179, 179),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20,),
                Text(data['time'],
                style: TextStyle(
                  fontSize: 66,
                  color:  Color.fromARGB(255, 180, 179, 179),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
