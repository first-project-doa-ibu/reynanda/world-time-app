import 'package:flutter/material.dart';
import 'package:world_time/service/world_time.dart';

class ChooseLoc extends StatefulWidget {
  const ChooseLoc({super.key});

  @override
  State<ChooseLoc> createState() => _ChooseLocState();
}

class _ChooseLocState extends State<ChooseLoc> {

  List<WorldTime> locations = [
    WorldTime(location: 'London', flag: 'uk.png', url: 'Europe/London'),
    WorldTime(location: 'New York', flag: 'usa.png', url: 'America/New_York'),
    WorldTime(location: 'Paris', flag: 'fr.png', url: 'Europe/Paris'),
    WorldTime(location: 'Tokyo', flag: 'jpn.png', url: 'Asia/Tokyo'),
    WorldTime(location: 'Sydney', flag: 'au.png', url: 'Australia/Sydney'),
    WorldTime(location: 'Dubai', flag: 'ae.png', url: 'Asia/Dubai'),
    WorldTime(location: 'Jakarta', flag: 'indonesia.png', url: 'Asia/Jakarta'),
  ];


  void updateTime(index) async{
    WorldTime instance = locations[index];
    await instance.getTime();
    Navigator.pop(context, {
      'location' : instance.location,
      'flag' : instance.flag,
      'time' : instance.time, 
      "isDaytime" : instance.isDaytime,
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        backgroundColor: Colors.blue[900],
        title: Text('Choose a Loc'),
        centerTitle: true,
        elevation: 0,
      ),
      body: ListView.builder(
        itemCount: locations.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 1.0, horizontal: 4),
            child: Card(
              child: ListTile(
                onTap: () {
                  updateTime(index);
                },
                title: Text(locations[index].location),
                leading: CircleAvatar(
                  backgroundImage: AssetImage('imgs/${locations[index].flag}'),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}